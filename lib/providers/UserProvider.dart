import 'package:flutter/cupertino.dart';
import 'package:freespace/model/UsersModel.dart';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class UserProvider extends ChangeNotifier {


  List<UsersModel> _items;
  List<UsersModel> get listitems {
    return _items;
  }


  set listitems(List<UsersModel> val) {
    _items = val;
    notifyListeners();
  }



//Get all Users
  Future<List<UsersModel>> fetchItems() async {
    http.Response response =
    await http.get("https://609b59ee2b549f00176e34df.mockapi.io/api/users");

    if (response.statusCode == 200) {
      var mapResponse = jsonDecode(response.body);
      List items = mapResponse.cast<Map<String, dynamic>>();
      List<UsersModel> dataAll = items.map<UsersModel>((json) {
        return UsersModel.fromJson(json);
      }).toList();
      listitems = dataAll;
      return listitems;
    } else {
      throw Exception('Failed to load from the Internet');
    }
  }

}


