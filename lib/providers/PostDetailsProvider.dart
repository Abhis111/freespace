import 'package:flutter/cupertino.dart';
import 'package:freespace/model/PostDetailsModel.dart';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class PostDetailsProvider extends ChangeNotifier {


  PostDetailsModel _items;
  PostDetailsModel get listitems {
    return _items;
  }


  set listitems(PostDetailsModel val) {
    _items = val;
    notifyListeners();
  }
//clear details
  void clearListitems() {
    _items = null;
    notifyListeners();
  }



//Get PostDetails
  Future<PostDetailsModel> fetchItems(String userId,String postId) async {
    http.Response response =
    await http.get("https://609b59ee2b549f00176e34df.mockapi.io/api/users/$userId/posts/$postId");

    if (response.statusCode == 200) {
      var mapResponse = jsonDecode(response.body);

      PostDetailsModel detailsModel = PostDetailsModel.fromJson(mapResponse);
      listitems = detailsModel;
      return listitems;
    } else {
      throw Exception('Failed to load from the Internet');
    }
  }

}


