import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:freespace/model/PostModel.dart';
import 'package:http/http.dart' as http;

class PostProvider extends ChangeNotifier {
  List<PostModel> _items = null;

  List<PostModel> get listitems {
    return _items;
  }

  set listitems(List<PostModel> val) {
    _items = val;
    notifyListeners();
  }
//Clear all posts
  void clearListitems() {
    _items = null;
    notifyListeners();
  }

//Get all Posts
  Future<List<PostModel>> fetchItems(String id) async {
    http.Response response = await http
        .get("https://609b59ee2b549f00176e34df.mockapi.io/api/users/$id/posts");

    if (response.statusCode == 200) {
      var mapResponse = jsonDecode(response.body);
      List items = mapResponse.cast<Map<String, dynamic>>();
      List<PostModel> dataAll = items.map<PostModel>((json) {
        return PostModel.fromJson(json);
      }).toList();
      listitems = dataAll;
      return listitems;
    } else {
      throw Exception('Failed to load from the Internet');
    }
  }

//Add a new Post
  Future addItem(String id, String postTitle, String postBody) async {
    Map<String, String> body = {
      'title': postTitle,
      'body': postBody,
    };

    // Map<dynamic, dynamic> mapData = toJson(item);
    // String newjson = json.encode(mapData);

    final response = await http.post(
        'https://609b59ee2b549f00176e34df.mockapi.io/api/users/$id/posts',
        body: body);
    if (response.statusCode == 201) {
      //  final responseBody = await json.decode(response.body);
      return SnackBar(content: Text(response.body));
    } else {
      print(response.body);
      throw Exception(
          'Failed to update the Item. Error: ${response.toString()}');
    }
  }
}
