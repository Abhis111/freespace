import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../lists/UserList.dart';
import '../providers/UserProvider.dart';

class HomeActivity extends StatefulWidget {
  HomeActivity({Key key}) : super(key: key);
  @override
  _HomeActivityState createState() => _HomeActivityState();
}

class _HomeActivityState extends State<HomeActivity> {
  UserProvider notifier;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final notifier = Provider.of<UserProvider>(context);

    if (this.notifier != notifier) {
      this.notifier = notifier;
      Future.microtask(() => notifier.fetchItems());
    }
  }

  @override
  Widget build(BuildContext context) {
    UserProvider blockitem = Provider.of<UserProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("User List"),
      ),
      body: blockitem.listitems == null
          ? Center(child: CircularProgressIndicator())
          : UserList(),

    );
  }
}
