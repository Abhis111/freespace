import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:freespace/activity/PostListActivity.dart';
import 'package:freespace/providers/PostProvider.dart';
import 'package:provider/provider.dart';

class AddPostActivity extends StatefulWidget {
  String postId = "";

  AddPostActivity({Key key, this.postId}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new _AddPostActivityState();
}

class _PostData {
  String postTitle = '';
  String postBody = '';
}

class _AddPostActivityState extends State<AddPostActivity> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  _PostData _data = new _PostData();

  String _validatePostTitle(String value) {
    if (value.isEmpty) {
      return "Please Enter Post Title";
    }
    return null;
  }

  String _validatePostBody(String value) {
    if (value.isEmpty) {
      return 'Please Enter Post Body';
    }

    return null;
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    var postProvider = Provider.of<PostProvider>(context);

    return WillPopScope(
      onWillPop: (){
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => PostListActivity(postId: widget.postId,),
          ),
        );
      },
      child: new Scaffold(
        appBar: new AppBar(
          title: new Text('Add Post'),
        ),
        body: new Container(
            padding: new EdgeInsets.all(20.0),
            child: new Form(
              key: this._formKey,
              child: new ListView(
                children: <Widget>[
                  new TextFormField(
                      keyboardType: TextInputType.text,
                      decoration: new InputDecoration(labelText: 'Post Title'),
                      validator: this._validatePostTitle,
                      onSaved: (String value) {
                        this._data.postTitle = value;
                      }),
                  new TextFormField(
                      minLines: 5,
                      maxLines: 5,
                      decoration: new InputDecoration(labelText: 'Post Body'),
                      validator: this._validatePostBody,
                      onSaved: (String value) {
                        this._data.postBody = value;
                      }),
                  new Container(
                    width: screenSize.width,
                    child: new RaisedButton(
                      child: new Text(
                        'Add Post',
                        style: new TextStyle(color: Colors.white),
                      ),
                      onPressed: () async {
                        if (this._formKey.currentState.validate()) {
                          _formKey.currentState.save(); // Save our form now.
                          await postProvider.addItem(
                              widget.postId, _data.postTitle, _data.postBody);

                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PostListActivity(postId: widget.postId,),
                            ),
                          );
                        }
                      },
                      color: Colors.blue,
                    ),
                    margin: new EdgeInsets.only(top: 20.0),
                  )
                ],
              ),
            )),
      ),
    );
  }
}
