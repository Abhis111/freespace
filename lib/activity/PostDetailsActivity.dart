import 'package:flutter/material.dart';
import 'package:freespace/providers/PostDetailsProvider.dart';
import 'package:provider/provider.dart';

class PostDetailsActivity extends StatefulWidget {
  String postId = "";
  String userId = "";

  PostDetailsActivity({Key key, this.postId, this.userId}) : super(key: key);

  @override
  _PostDetailsActivityState createState() => _PostDetailsActivityState();
}

class _PostDetailsActivityState extends State<PostDetailsActivity> {
  PostDetailsProvider notifier;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final notifier = Provider.of<PostDetailsProvider>(context);

    if (this.notifier != notifier) {
      this.notifier = notifier;

      Future.microtask(() => notifier.clearListitems());
      Future.microtask(() => notifier.fetchItems(widget.userId, widget.postId));
    }
  }

  @override
  Widget build(BuildContext context) {
    PostDetailsProvider blockitem = Provider.of<PostDetailsProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Post Details"),
      ),
      body: blockitem.listitems == null
          ? Center(child: CircularProgressIndicator())
          : Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8),
                        child: Text(
                          blockitem.listitems.title.toUpperCase(),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Text(
                          blockitem.listitems.body,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
