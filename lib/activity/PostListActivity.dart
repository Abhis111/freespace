import 'package:flutter/material.dart';
import 'package:freespace/activity/AddPostActivity.dart';
import 'package:freespace/lists/PostList.dart';
import 'package:provider/provider.dart';

import '../lists/UserList.dart';
import '../providers/PostProvider.dart';

class PostListActivity extends StatefulWidget {

  String postId = "";
  String userName = "";
  PostListActivity({Key key, this.postId})
      : super(key: key);


  @override
  _PostListActivityState createState() => _PostListActivityState();
}

class _PostListActivityState extends State<PostListActivity> {
  PostProvider notifier;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final notifier = Provider.of<PostProvider>(context);

    if (this.notifier != notifier) {
      this.notifier = notifier;

      Future.microtask(() => notifier.clearListitems());
      Future.microtask(() => notifier.fetchItems(widget.postId));
    }
  }


  @override
  Widget build(BuildContext context) {
    PostProvider blockitem = Provider.of<PostProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Post List"),
      ),
      body: blockitem.listitems == null
          ? Center(child: CircularProgressIndicator())
          : PostList( postId: widget.postId,),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => AddPostActivity(postId: widget.postId,),
            ),
          );
           },
        child: Icon(Icons.add),
      ),
    );
  }
}
