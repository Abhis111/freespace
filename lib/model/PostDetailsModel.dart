class PostDetailsModel {
  String _id;
  String _author;
  String _title;
  String _body;

  PostDetailsModel({String id, String author, String title, String body}) {
    this._id = id;
    this._author = author;
    this._title = title;
    this._body = body;
  }

  String get id => _id;
  set id(String id) => _id = id;
  String get author => _author;
  set author(String author) => _author = author;
  String get title => _title;
  set title(String title) => _title = title;
  String get body => _body;
  set body(String body) => _body = body;

  PostDetailsModel.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _author = json['author'];
    _title = json['title'];
    _body = json['body'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['author'] = this._author;
    data['title'] = this._title;
    data['body'] = this._body;
    return data;
  }
}
