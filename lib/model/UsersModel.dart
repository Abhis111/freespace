class UsersModel {
  String _id;
  String _name;
  String _avatar;
  List<Posts> _posts;

  UsersModel({String id, String name, String avatar, List<Posts> posts}) {
    this._id = id;
    this._name = name;
    this._avatar = avatar;
    this._posts = posts;
  }

  String get id => _id;
  set id(String id) => _id = id;
  String get name => _name;
  set name(String name) => _name = name;
  String get avatar => _avatar;
  set avatar(String avatar) => _avatar = avatar;
  List<Posts> get posts => _posts;
  set posts(List<Posts> posts) => _posts = posts;

  UsersModel.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _avatar = json['avatar'];
    if (json['posts'] != null) {
      _posts = new List<Posts>();
      json['posts'].forEach((v) {
        _posts.add(new Posts.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['avatar'] = this._avatar;
    if (this._posts != null) {
      data['posts'] = this._posts.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Posts {
  String _id;
  String _author;
  String _title;
  String _body;

  Posts({String id, String author, String title, String body}) {
    this._id = id;
    this._author = author;
    this._title = title;
    this._body = body;
  }

  String get id => _id;
  set id(String id) => _id = id;
  String get author => _author;
  set author(String author) => _author = author;
  String get title => _title;
  set title(String title) => _title = title;
  String get body => _body;
  set body(String body) => _body = body;

  Posts.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _author = json['author'];
    _title = json['title'];
    _body = json['body'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['author'] = this._author;
    data['title'] = this._title;
    data['body'] = this._body;
    return data;
  }
}
