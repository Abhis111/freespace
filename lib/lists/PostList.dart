import 'package:flutter/material.dart';
import 'package:freespace/activity/PostDetailsActivity.dart';
import 'package:freespace/activity/PostDetailsActivity.dart';
import 'package:provider/provider.dart';

import '../activity/PostListActivity.dart';
import '../providers/PostProvider.dart';

class PostList extends StatelessWidget {
  String postId = "";
  PostList( {Key key, this.postId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final PostProvider blockitem = Provider.of<PostProvider>(context);



    return ListView.builder(
      itemCount: blockitem.listitems.length,
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          onTap: (){
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PostDetailsActivity(
                    postId: blockitem.listitems[index].id,
                    userId: postId,
                  )),
            );
          },
          child: Card(
            elevation: 2.0,
            color: Colors.blue.shade50,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,


                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Text(blockitem.listitems[index].title,style: TextStyle(
                      fontSize: 20
                          ,
                      fontWeight: FontWeight.bold
                    ),),
                  ),
                  Text(blockitem.listitems[index].body),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
