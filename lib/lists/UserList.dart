import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../activity/PostListActivity.dart';
import '../providers/UserProvider.dart';

class UserList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final UserProvider blockitem = Provider.of<UserProvider>(context);



    return ListView.builder(
      itemCount: blockitem.listitems.length,
      itemBuilder: (BuildContext context, int index) {
        return Card(
          elevation: 2.0,
          color: Colors.blue.shade50,
          child: ListTile(
            leading: Container(
              width: 50,
              height: 50,
              child:  Center(
                child: FadeInImage.assetNetwork(
                  placeholder: 'assets/images/user.png',
                  image: blockitem.listitems[index].avatar,
                ),
              ),
            ),
            title: Text(blockitem.listitems[index].name),
            trailing: Icon(
              Icons.arrow_forward_ios,
              color: Colors.green.shade400,
            ),
            onTap: () {

              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PostListActivity(
                      postId: blockitem.listitems[index].id,
                    )),
              );

            },
          ),
        );
      },
    );
  }
}
