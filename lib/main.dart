import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:freespace/providers/PostProvider.dart';
import 'package:freespace/providers/PostDetailsProvider.dart';
import 'package:freespace/providers/UserProvider.dart';
import 'package:provider/provider.dart';

import 'activity/HomeActivity.dart';

final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();
GlobalKey<NavigatorState> navigatorKey =
GlobalKey(debugLabel: "Main Navigator");

void main() async {
  runApp(
    MultiProvider(
      providers: [

        ChangeNotifierProvider(create: (_) => UserProvider()),
        ChangeNotifierProvider(create: (_) => PostDetailsProvider()),
        ChangeNotifierProvider(create: (_) => PostProvider()),
      ],

      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        navigatorKey: navigatorKey,
        title: "FreeSpace",
        theme: ThemeData(
          primaryColor: Colors.greenAccent,
          //accentColor: Colors.orangeor iOS is only supported on the Mac.
          //Process finished with exit code 1
        ),
        home: HomeActivity(),
        navigatorObservers: [routeObserver],
        ),
    ),
  );
}